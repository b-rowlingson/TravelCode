#!/bin/env

# These are standard python packages:
import sys
import csv
from datetime import datetime, timedelta
import requests.packages.urllib3 as u3
u3.disable_warnings()
import collections
import functools 

# You need to install these:
import xlrd # reading Excel spreadsheets
import dateutil.parser # parsing times like "2017-05-23 09:00:00"
import googlemaps # doing travel time queries
from taskr import task # for making easily callable scripts

#
# This saves us calling the API twice for the same query
#
class memoized(object):
   '''Decorator. Caches a function's return value each time it is called.
   If called later with the same arguments, the cached value is returned
   (not reevaluated).
   '''
   def __init__(self, func):
       self.func = func
       self.cache = {}
       self.cachehits = 0
       self.cachemiss = 0
       self.cantcache = 0
   def __call__(self, *args):
       if not isinstance(args, collections.Hashable):
           # uncacheable. a list, for instance.
           # better to not cache than blow up.           
           self.cantcache = self.cantcache + 1
           return self.func(*args)
       if args in self.cache:
          self.cachehits = self.cachehits + 1
          return self.cache[args]
       else:
          self.cachemiss = self.cachemiss + 1
          value = self.func(*args)
          self.cache[args] = value
          return value
   def __repr__(self):
        '''Return the function's docstring.'''
        return self.func.__doc__
   def __get__(self, obj, objtype):
        '''Support instance methods.'''
        return functools.partial(self.__call__, obj)


#
# get and set a google client API key
#
def get_client(apikey):
    key = file(apikey).read().strip()
    gc = googlemaps.Client(key)
    return gc

@memoized
def get_journey(client, from_location, to_location, mode, arrive):
    if len(from_location)<5:
        raise ValueError,"Bad 'from' postcode '%s' " % from_location
    if len(to_location) < 5:
        raise ValueError,"Bad 'to' postcode '%s' " % to_location 

    journey = client.directions(from_location, to_location, mode=mode, 
                      arrival_time=arrive)    

    if len(journey) == 0:
        raise ValueError,"Journey has no legs (bad postcode?)"
    if len(journey)!=1:
        raise ValueError,"Journey does not have one leg"
        
    leg = journey[0]['legs'][0]
    duration = leg['duration']['value']
    distance = leg['distance']['value']
    start = leg['start_location']
    end = leg['end_location']
    
    try:
        leave_house_time = datetime.fromtimestamp(leg["departure_time"]['value'])
    except:
        leave_house_time = arrive - timedelta(seconds=duration)

    trip_duration = (arrive - leave_house_time).total_seconds()
    leave_house = str(leave_house_time)
#    return(journey)
    return {'duration':duration, 
            "distance": distance,
            "departure": leave_house,
            "total_trip": trip_duration,
            "start": start,
            "end": end,
            "journey": journey,
            "leg": leg,
            "arrive": arrive
        }

def cachestats(stream):
    print >>stream,"Hits:", get_journey.cachehits
    print >>stream,"Miss:", get_journey.cachemiss
    print >>stream,"Fail:", get_journey.cantcache

def bad_journey():
    j = {}
    j['duration']=-1
    j['distance']=-1
    j['departure']=0
    j['total_trip']=0
    j['start']={}
    j['start']['lat']=0
    j['start']['lng']=0
    j['end']={}
    j['end']['lat']=0
    j['end']['lng']=0
    return j


@task
@task.set_argument("xlspath", help="Path of Excel Spreadsheet to read")
@task.set_argument("apifile", help="Path to file containing API key")
@task.set_argument("arrival_time", help="Requested arrival time, eg '2017-05-25 09:00:00'")
@task.set_argument("first_row",type=int, help="First data row to process")
@task.set_argument("last_row",type=int, help="Last data row to process")
@task.set_argument("from_column",type=int, help="Column index of source postcode")
@task.set_argument("to_column",type=int, help="Column index of destination postcode")
@task.set_argument("--mode","-m",help = "Transport mode type", choices=("transit","walking","bicycling","driving"))

def xlstransit(xlspath,  apifile, arrival_time, first_row, last_row, from_column, to_column, mode="transit"):
    key = file(apifile).read().strip() 
    gc = googlemaps.Client(key)

    arrival_time_d = dateutil.parser.parse(arrival_time)
    wb = xlrd.open_workbook(xlspath)
    s = wb.sheets()[0]
    first_row = max(1, first_row)
    last_row = min(s.nrows-1, last_row)
    print >>sys.stderr,"Doing ",first_row," to ",last_row
    for row in range(first_row, last_row+1):
        print >> sys.stderr,"row ",row, "/",last_row,
        pcfrom = s.cell(row,from_column).value
        pcto = s.cell(row,to_column).value
        print >> sys.stderr,"   from: ",pcfrom," to: ",pcto
        try:
            journey = get_journey(gc, pcfrom, pcto, mode, arrival_time_d)
            status = "ok"
        except:
            e = sys.exc_info()
            print >>sys.stderr, e[1]
            journey = bad_journey()
            status = str(e[1])
        outputs =  (
            ('row', row),
            ('from', pcfrom), 
            ('to', pcto),
            ('leave_house', journey['departure']),  # leave-house time
            ('due_time', arrival_time),            # appointment time
            ('moving_time', journey['duration']),    # on-the-move-time
            ('leave_to_due', journey['total_trip']),# house-to-appointment time
            ('distance', journey['distance']),
            ('lat0', journey['start']['lat']),('lng0',journey['start']['lng']),
            ('lat1', journey['end']['lat']),('lng1', journey['end']['lng']),
            ('status', status)
        )
        if row == first_row:
            print "\t".join(["\"%s\""]*len(outputs)) % tuple(v[0] for v in outputs)
        print "\t".join(["\"%s\""]*len(outputs)) % tuple(v[1] for v in outputs)

    #cachestats(sys.stderr)
    
    
if __name__ == '__main__':
    task.dispatch()
