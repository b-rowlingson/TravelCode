# Computing Travel Times

The python script in this project will read an Excel spreadsheet and use the 
google API to work out the travel time from a source to a destination postcode,
to arrive for a specified time.

## Installation:

You will need Python and a number of additional packages. To work in a virtual
environment, try something like the following:

```
virtualenv travel
. travel/bin/activate
pip install xlrd
pip install googlemaps
pip install taskr
pip install python-dateutil
```

## API Key:

You need to get an API key to access the google travel API. This is a 
random character string of about 40 letters and numbers that you can get
from google. Put it in a file in a safe place. You will have to refer to
the path of this file in further usage.

## Preparation:

Your spreadsheet should have a one-line header and have source and 
destination columns containing UK postcodes, like the following:

| Name        | From    | To      | etc   |
|-------------|---------|---------|-------|
| Fred Bloggs | LA1 3RH | LA1 4YF |       |
| Anne Other  | LA2 9AY | LA1 4YF |       |
| Bea Careful | LA3 9PT | LA1 4YF |       |

Missing data will be handled gracefully by the script.

## Usage:

The usage for the  main routine, `xlstransit`, can be seen by using the `-h` flag:

```
$ python getroute.py xlstransit -h

usage: getroute.py xlstransit [-h]
                              [--mode {transit,walking,bicycling,driving}]
                              xlspath apifile arrival_time first_row last_row
                              from_column to_column

positional arguments:
  xlspath               Path of Excel Spreadsheet to read
  apifile               Path to file containing API key
  arrival_time          Requested arrival time, eg '2017-05-25 09:00:00'
  first_row             First data row to process
  last_row              Last data row to process
  from_column           Column index of source postcode
  to_column             Column index of destination postcode

optional arguments:
  -h, --help            show this help message and exit
  --mode {transit,walking,bicycling,driving}, -m {transit,walking,bicycling,driving}
                        Transport mode type (default: transit)
```

Note that the first rows and columns in a spreadsheet are counted as *zero*, and usually have row
or column headers. A spreadsheet of the data above is stored in the `Test` folder, and can be 
run this way:

```
python getroute.py xlstransit ./Test/simple.xlsx  ../API/api-project.txt "2017-05-24 09:00:00" 1 99 1 2 > Test/simple_out.csv
```

The code will log each line as it computes the travel time. In the above example there are only 
three data rows in the table, but using 99 as the last row will not cause an error - the code
will stop at the final row in the data.

That example will write its results to the file `Test/simple_out.csv`. This is a tab-separated
value file with a header, and all fields quoted. 

```
"row"	"from"	"to"	"leave_house"	"due_time"	"moving_time"	"leave_to_due"	"distance"	"lat0"	"lng0"	"lat1"	"lng1"	"status"
"1"	"LA1 3RH"	"LA1 4YF"	"2017-05-24 08:32:15"	"2017-05-24 09:00:00"	"1378"	"1665.0"	"5625"	"54.0434552"	"-2.7951859"	"54.008807"	"-2.7849292"	"ok"
"2"	"LA2 9AY"	"LA1 4YF"	"2017-05-24 07:43:56"	"2017-05-24 09:00:00"	"3917"	"4564.0"	"20729"	"53.9781275"	"-2.7339391"	"54.008807"	"-2.7849292"	"ok"
"3"	"LA3 3PT"	"LA1 4YF"	"2017-05-24 07:57:31"	"2017-05-24 09:00:00"	"2442"	"3749.0"	"9382"	"54.0568622"	"-2.8357769"	"54.008807"	"-2.7849292"	"ok"
```

The fields are as follows:

 * `row` : the row number in the spreadsheet. Use this to match the data back to the spreadsheet rows.
 * `from`, `to` : copied from the spreadsheet.
 * `leave_house` : time at which the person needs to leave the `from` address to make the due time.
 * `due_time` : the time specified on the command line for arrival by.
 * `moving_time` : time in seconds from the leaving time to the arrival time. For public transport, the arrival time may have to be substantially 
                   before the due time because of transport schedules.
 * `leave_to_due` : time between leaving the house and the due time. 
 * `distance` : distance travelled along the path of the transport, not the straight-line distance between the two points.
 * `lat0`, `lng0`, `lat1`, `lng1` : coordinates of start and finish in WGS84 (EPSG:4326) coordinates.
 * `status` : a string - this will be `"ok"` if the transport API returned a journey that was then parsed and data extracted correctly - and an 
                    error message if something else happened. The other values in the record are not trustable if this field is not `"ok"`.


## Paging

The code has the `first_row` and `last_row` parameters so you can run batched jobs. 
Google's API may prevent you from running 10000 queries, so you can use the row
specification to run 1000 queries at a time. For example:

```
python getroute.py xlstransit bigdata.xlsx  api.txt "2017-05-24 09:00:00"     1 999 1 2   > out_1.csv
python getroute.py xlstransit bigdata.xlsx  api.txt "2017-05-24 09:00:00" 1000 1999 1 2   > out_2.csv
python getroute.py xlstransit bigdata.xlsx  api.txt "2017-05-24 09:00:00" 2000 2999 1 2   > out_3.csv
```

and so on. The resulting CSV files can be contatenated and then merged with the spreadsheet.

## Failures

The `Test` folder contains `lancaster.xlsx` which has a deliberately bad postcode. In this case you will
see the `status` column contains `"Journey has no legs (bad postcode?)" and most of the other values
are 0 or -1.


